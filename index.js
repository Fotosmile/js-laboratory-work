import {Vector} from "js-laboratory-work";

const random_max = 1000;
const wasm_element = document.getElementById("wasm_runtime");
const js_element = document.getElementById("js_runtime");
const wasm_window = document.getElementById("wasm_window");
const js_window = document.getElementById("js_window");
const result_text = document.getElementById("run_result");
const run_me_button = document.getElementById("run_me_button");
const clear_results_button = document.getElementById("clear_result_button");
const length_of_vectors = document.getElementById("length_of_vectors");
const red_color_name = "red";
const green_color_name = "green";
const yellow_color_name = "yellow";
const light_grey_color_name = "lightgrey";

function random_array(array_length, max) {
    return Array.from({length: array_length}, () => Math.random() * max);
}

function measure_performance(f) {
    const now = performance.now();
    try {
        f();
    } catch(e) {
        console.error(e);
    }

    return performance.now() - now;
}

function js_multiply_arrays(a1, a2) {
    if (a1.length != a2.length) {
        throw "Sizes are different";
    }

    var result = 0;
    for (var i = 0; i < a1.length; i++) {
        result += a1[i] * a2[i];
    }

    return result;
}

function run() {
    clear_all();

    const length = length_of_vectors.value;
    if (length == "") {
        alert("Input length of vectors!");
        return;
    }
    const array1 = random_array(length, random_max);
    const array2 = random_array(length, random_max);

    const vector1 = Vector.new(new Float64Array(array1));
    const vector2 = Vector.new(new Float64Array(array2));

    const wasm_runtime = measure_performance(function() {
        vector1.mul(vector2);
    });
    const js_runtime = measure_performance(function() {
        js_multiply_arrays(array1, array2);
    });

    if (wasm_runtime < js_runtime) {
        wasm_window.style.backgroundColor = green_color_name;
        js_window.style.backgroundColor = red_color_name;
        result_text.innerHTML = "WASM implementation is ".concat(100 - (wasm_runtime * 100)/ js_runtime , "% faster. WASM is always faster!");
    } else if (wasm_runtime > js_runtime) {
        wasm_window.style.backgroundColor = red_color_name;
        js_window.style.backgroundColor = green_color_name;
        result_text.innerHTML = "Well.. This time JS implementation is ".concat(100 - (js_runtime * 100)/ wasm_runtime , "% faster. Maybe there is a bug?");
    } else {
        wasm_window.style.backgroundColor = yellow_color_name;
        js_window.style.backgroundColor = yellow_color_name;
        result_text.innerHTML = "Results are equal. Let's run it again!";
    }

    wasm_element.innerHTML = wasm_runtime + "ms";
    js_element.innerHTML = js_runtime + "ms";
}

function clear_all() {
    wasm_window.style.backgroundColor = light_grey_color_name;
    js_window.style.backgroundColor = light_grey_color_name;
    result_text.innerHTML = "";
    wasm_element.innerHTML = "0ms";
    js_element.innerHTML = "0ms";
}

run_me_button.onclick = run;
clear_results_button.onclick = clear_all;
