use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Debug)]
pub struct Vector(Vec<f64>);

#[wasm_bindgen]
impl Vector {
    pub fn new(v: Vec<f64>) -> Self {
        Self(v)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn to_string(&self) -> String {
        format!("{:?}", self)
    }

    pub fn mul(&self, other: &Vector) -> f64 {
        assert_eq!(self.0.len(), other.0.len());

        self.0
            .iter()
            .zip(other.0.iter())
            .map(|(e1, e2)| *e1 * *e2)
            .sum()
    }
}
