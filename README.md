<div align="center">

  <h1><code>JS laboratory work</code></h1>

## How to run

```bash
npm init wasm-app www &&
cp {index.html,index.js,package.json} www/ &&
cd www/ &&
npm install &&
npm run start
```

## How to try

Navigate in your browser `localhost:8080` (you should see the page like on the image). Enjoy.
![](img/in_browser.png)
